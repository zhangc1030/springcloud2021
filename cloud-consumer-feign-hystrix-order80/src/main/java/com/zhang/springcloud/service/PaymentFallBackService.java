/*
 * FileName: PaymentFallBackService.java
 * Author:   zhangc
 * Date:     2021/10/18 21:30
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhang.springcloud.service;

import org.springframework.stereotype.Component;

/**
 * 功能描述:<br>
 *
 * @author zhangc
 */
@Component
public class PaymentFallBackService implements PaymentHystrixService {
    @Override
    public String paymentInfo_OK(Integer id) {
        return "服务调用失败1，提示来自：cloud-consumer-feign-order80";
    }

    @Override
    public String paymentInfo_TimeOut(Integer id) {
        return "服务调用失败2，提示来自：cloud-consumer-feign-order80";
    }
}

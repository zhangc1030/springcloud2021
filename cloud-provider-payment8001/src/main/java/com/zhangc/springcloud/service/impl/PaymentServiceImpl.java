/*
 * FileName: PaymentServiceImpl.java
 * Author:   zhangc
 * Date:     2021/10/8 20:31
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.springcloud.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.zhangc.springcloud.dao.PaymentDao;
import com.zhangc.springcloud.entities.Payment;
import com.zhangc.springcloud.service.PaymentService;

/**
 * 功能描述:<br>
 *
 * @author zhangc
 */
@Service
public class PaymentServiceImpl implements PaymentService {
    @Resource
    private PaymentDao paymentDao;

    @Override
    public int create(Payment payment) {
        return paymentDao.create(payment);

    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentDao.getPaymentById(id);
    }
}

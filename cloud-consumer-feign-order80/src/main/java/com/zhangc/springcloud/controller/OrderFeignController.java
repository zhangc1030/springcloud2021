/*
 * FileName: OrderFeignController.java
 * Author:   zhangc
 * Date:     2021/10/14 22:43
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.springcloud.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.zhangc.springcloud.entities.CommonResult;
import com.zhangc.springcloud.entities.Payment;
import com.zhangc.springcloud.service.PaymentFeignService;

/**
 * 功能描述:<br>
 *
 * @author zhangc
 */
@RestController
public class OrderFeignController {
    @Resource
    private PaymentFeignService paymentFeignService;

    @GetMapping(value = "/consumer/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id) {
        return paymentFeignService.getPaymentById(id);
    }

}

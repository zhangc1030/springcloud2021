/*
 * FileName: LoadBalancer.java
 * Author:   zhangc
 * Date:     2021/10/14 21:22
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.springcloud.lb;

import java.util.List;

import org.springframework.cloud.client.ServiceInstance;

/**
 * 功能描述:<br>
 *
 * @author zhangc
 */
public interface LoadBalancer {
    ServiceInstance instances(List<ServiceInstance> serviceInstances);
}

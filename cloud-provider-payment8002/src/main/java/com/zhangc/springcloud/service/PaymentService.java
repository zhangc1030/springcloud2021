/*
 * FileName: PaymentService.java
 * Author:   zhangc
 * Date:     2021/10/8 20:29
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.springcloud.service;

import com.zhangc.springcloud.entities.Payment;

/**
 * 功能描述:<br>
 *
 * @author zhangc
 */
public interface PaymentService {
    public int create(Payment payment);

    public Payment getPaymentById(Long id);
}

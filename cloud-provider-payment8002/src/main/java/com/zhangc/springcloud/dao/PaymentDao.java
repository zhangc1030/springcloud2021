/*
 * FileName: PaymentDao.java
 * Author:   zhangc
 * Date:     2021/10/8 20:17
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名    修改时间    版本号       描述
 */
package com.zhangc.springcloud.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zhangc.springcloud.entities.Payment;

/**
 * 功能描述:<br>
 *
 * @author zhangc
 */
@Mapper
public interface PaymentDao {
    public int create(Payment payment);

    public Payment getPaymentById(@Param("id") Long id);
}
